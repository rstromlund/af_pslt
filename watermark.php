<?php

/*
Messages:

<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
	<channel>
		<title>Watermark Audio</title>
		<link>http://www.watermark.org/resources/messages</link>
		<description>Watermark exists to be and make more fully devoted followers of Christ</description>
		<language>en</language>
		<copyright>Watermark Community Church</copyright>
		<ttl>720</ttl>
		<image>
			<url>http://www.watermark.org/favicon.ico</url>
			<title>Watermark Audio: Recovery Channel</title>
			<link>http://www.watermark.org/</link>
		</image>
		<item>
			<title>podcast http://s3.amazonaws.com/Wccaudio/20190324.mp3</title>
			<link>http://s3.amazonaws.com/Wccaudio/20190324.mp3</link>
			<enclosure url="http://s3.amazonaws.com/Wccaudio/20190324.mp3" length="" type="audio/mpeg"/>
			<guid>http://s3.amazonaws.com/Wccaudio/20190324.mp3</guid>
			<pubDate>Sun, 24 Mar 2019 12:00:00 -0500</pubDate>
			<dc:creator>Todd Wagner</dc:creator>
			<category>sermons</category>
			<category>messages</category>
			<category>talks</category>
			<category>sunday</category>
			<category>lessons</category>
			<category>teaching</category>
			<category>preaching</category>
			<category>sermon series</category>
			<category>series</category>
			<category>leadership</category>
			<category>worship services</category>
			<category>resources</category>
			<category>Todd Wagner</category>
			<category>JP</category>
			<category>Jonathan Pokluda</category>
			<category>how do I watch messages</category>
			<category>how do I watch sermons</category>
			<description>podcast http://s3.amazonaws.com/Wccaudio/20190324.mp3</description>
		</item>
	</channel>
</rss>

*/

class Watermark extends Pslt_Processor {

	//E.g. : http://www.watermark.org/resources/messages
	//E.g. : http://www.watermark.org/series/120

	public $audio = array();

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;
		$_class = (XML_ELEMENT_NODE === $node->nodeType) ? $node->getAttribute('class') : 'n/a';

		// <html>...</html>
		if ('html' === $_nodeName && XML_ELEMENT_NODE === $node->nodeType) {
			array_push($this->outdom, '<?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/"><channel><link>' . $this->config_info['url'] . '</link><description>Watermark exists to be and make more fully devoted followers of Christ</description><language>en</language><copyright>Watermark Community Church</copyright><ttl>720</ttl><image><url>http://www.watermark.org/favicon.ico</url><title>Watermark Community Church</title><link>http://www.watermark.org/</link></image>');
			$r = parent::template($node, $mode);
			array_push($this->outdom, '</channel></rss>');

		}

		// head/title
		else if ('title' === $_nodeName && 'head' === $node->parentNode->nodeName) {
			array_push($this->outdom, '<title><![CDATA[' . trim($node->nodeValue) . ']]></title><description><![CDATA[' . trim($node->nodeValue) . ']]></description>');
			// No need to recurse this leaf node.

		}

		// (messages) button[contains(@data-audio, '.mp3') and contains(@data-audio, 'http')]
		else if ('button' === $_nodeName && ($nmp3 = strpos($node->getAttribute('data-audio'), '.mp3')) !== false && ($nhttp = strpos($node->getAttribute('data-audio'), 'http')) !== false) {
			$media = str_replace('\\', '', substr($node->getAttribute('data-audio'), $nhttp, $nmp3 - $nhttp + 4));

			// xsl:for-each-group (i.e. only unique media is listed)
			if (in_array($media, $this->audio)) return($r);
			array_push($this->audio, $media);

			preg_match('/(20\d\d)([01]\d)([0123]\d)/', $media, $d);
			$yr = $d[1];
			$mn = $d[2];
			$dy = $d[3];
			$date = date_create("$yr-$mn-$dy 12:00PM")->format(DATE_RFC822);

			$title = "podcast $media";
			$href = $media;
			$creator = $this->xpath->query('(/html/body//a[contains(@href, "/person")])[1]/text()', $node)[0]->nodeValue;

			array_push($this->outdom, "<item><title><![CDATA[$title]]></title><link>$href</link><guid>$href</guid><enclosure url=\"$href\" length=\"\" type=\"audio/mpeg\" /><pubDate>$date</pubDate><dc:creator><![CDATA[$creator]]></dc:creator><description><![CDATA[$title]]></description><category>sermons</category><category>messages</category><category><![CDATA[" . mb_strtolower($creator). "]]></category></item>");

		}

		// (regen) a['thumbnail' = @class and starts-with(@href, 'http') and contains(@href, '://')])[15 >= position()]
		else if ('a' === $_nodeName && 'thumbnail' === $_class && $node->getAttribute('href')) {
			$datestr = $this->xpath->query('.//text()[contains(., "/20")]', $node)[0]->nodeValue;
			$date = date_create((isset($datestr) ? $datestr : 'now') . ' 12:00PM')->format(DATE_RFC822);

			$title = $this->xpath->query('..//h4/text()', $node)[0]->nodeValue;
			$href = $node->getAttribute('href');
			$creator = substr($title, 0, strpos($title, ' -'));

			$cat = "<category>recovery</category><category>re:generation</category>";
			array_map(
				function ($c) use (&$cat) {$cat .= '<category>' . mb_strtolower(trim($c)) . '</category>';},
				explode(',', str_replace('&', ',', substr($title, strpos($title, '- ') + 2)))
			);

			array_push($this->outdom, "<item><title><![CDATA[$title]]></title><link>$href</link><guid>$href</guid><pubDate>$date</pubDate><dc:creator><![CDATA[$creator]]></dc:creator><description><![CDATA[$title]]></description>$cat</item>");

			//NOTE: need to filter these articles to load each page and look for media links.

		}

		else {
			$r = parent::template($node, $mode);

		}

		return($r);

	}

	/*
	Message: {
			"owner_uid": "1",
			"guid": "1,http://s3.amazonaws.com/Wccaudio/20190324.mp3",
			"guid_hashed": "SHA1:595eea242001acb19d3f7cfd4aa24c2842ed03a2",
			"title": "podcast http://s3.amazonaws.com/Wccaudio/20190324.mp3",
			"content": "podcast http://s3.amazonaws.com/Wccaudio/20190324.mp3",
			"link": "http://s3.amazonaws.com/Wccaudio/20190324.mp3",
			"labels": [],
			"tags": {
				"0": "sermons",
				"1": "messages",
				"2": "todd wagner",
				"6": "recovery"
			},
			"author": "Todd Wagner",
			"force_catchup": false,
			"score_modifier": 0,
			"language": "en",
			"num_comments": 0,
			"feed": {
				"id": 341,
				"fetch_url": "http://pslt.localhost/watermark?url=http://www.watermark.org/resources/messages",
				"site_url": "http://www.watermark.org/resources/messages",
				"cache_images": "0"
			}
		}

	Regen: {
			"owner_uid": "1",
			"guid": "1,http://www.watermark.org/message/1601",
			"guid_hashed": "SHA1:84091e8a13c9763da8a082ed182f2d809d629940",
			"title": "Nate - Changes to Watermark's Recovery Ministry",
			"content": "Nate - Changes to Watermark's Recovery Ministry",
			"link": "http://www.watermark.org/message/1601",
			"labels": [],
			"tags": {
				"0": "recovery",
				"1": "re:generation",
				"2": "changes to watermark's recovery ministry",
				"5": "changes to watermarks recovery ministry"
			},
			"author": "Nate",
			"force_catchup": false,
			"score_modifier": 0,
			"language": "en",
			"num_comments": 0,
			"feed": {
				"id": 342,
				"fetch_url": "http://pslt.localhost/watermark?url=http://www.watermark.org/series/120",
				"site_url": "http://www.watermark.org/series/120",
				"cache_images": "0"
			}
		}
	*/

	function article_filter($article) {
		// No mp3 in the link?  Must need a page load and further processing:
		if (strpos($article['link'], '.mp3') === false) {
			global $fetch_last_content_type;
			$doc = new DOMDocument();
			$link = trim($article['link']);

			$doc->loadHTML((version_compare(VERSION, '1.7.9', '>=')) ? fetch_file_contents($link) : file_get_contents($link));
			$xpath = new DOMXPath($doc);

			$media = $xpath->query('/html/head/meta["og:audio" = @property][1]/@content')[0]->nodeValue;
			if (isset($media)) {
				$article['link'] = $media;
				$article['content'] = $media;
				_debug("INFO ::: Set $article[title] link/content to $media");
			}

		}

		return $article;
	}

}
