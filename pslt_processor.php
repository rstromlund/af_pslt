<?php

abstract class Pslt_Processor {

	public $feed;
	public $fetch_url;
	public $fetch_comp;
	public $config_info;

	public $xpath;
	public $outdom;

	public function template(DOMNode $node, $mode) {
		$r = true;

		if ($node instanceof DOMText) {
			// XSLT normally would copy text nodes to the output dom; but I prefer to choose when to do that instead of overriding default behavior.

		}

		else {
			$r = $this->apply_templates($node->childNodes, $mode);

		}

		return $r;

	}

	function apply_templates($node, $mode) {
		//Need this debug from time to time.
		//_debug("pslt_processor.php@apply_templates: $node->nodeName, $mode, $node->nodeType, " . (XML_ELEMENT_NODE == $node->nodeType ? $node->getLineNo() : -1));

		$r = true;

		if (! isset($node)) {
			// do nothing

		}

		else if ($node instanceof DOMNodeList) {
			if ($node->length > 0) {
				foreach ($node as $n) {
					$r = $this->apply_templates($n, $mode);
				}
			}

		}

		else {
			$r = $this->template($node, $mode);

		}

		return $r;

	}

	function article_filter($article) { return $article; } // Placeholder, do no altering.

	public function init($feed, $fetch_url, $fetch_comp, $config_info) {
		$this->feed	 			= $feed;
		$this->fetch_url		= $fetch_url;
		$this->fetch_comp		= $fetch_comp;
		$this->config_info	= $config_info;

		$this->outdom			= array();
	}

	function process() {
		return $this->process_url($this->config_info['url'], '#default');
	}

	public function process_url($url, $mode) {
		_debug("process_url: Loading SGML from : $url");
		$sgml = fetch_file_contents(array('url' => $url));
		# Useful for feeds w/ non-CDATA protected HTML entities in XML (tsk, tsk):
		if (isset($this->config_info['entity_decode'])) {$sgml = html_entity_decode($sgml);}
		_debug("process_url: fetched bytes = " . mb_strlen($sgml));

		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		libxml_use_internal_errors(true);

		if (preg_match('/^.{1,200}<html/si', $sgml)) {
			// In the first 200-ish characters we found an opening HTML tag; treat as HTML
			_debug('process_url: treating as HTML');
			$doc->loadHTML('<?xml encoding="UTF-8">' . $sgml);

		} else {
			// Failing that, treat as XML
			_debug('process_url: treating as XML');
			$doc->loadXML($sgml);

		}

		$r = $this->process_doc($doc, $mode);
		if (! $r) _debug('ERROR process_url: process_doc returned false!');

		//FIXME: this ERROR message does not return valid RSS/Atom XML. Create a fake <item> w/ the error? Ignore the false return?
		return ($r) ? join('', $this->outdom) : "ERROR processing $url :: $mode :: " . join('', $this->outdom) . " :: $sgml";
	}

	public function process_doc($doc, $mode) {
		$this->xpath = new DOMXpath($doc);
		$this->xpath->registerNamespace('atom', 'http://www.w3.org/2005/Atom');
		return $this->apply_templates($doc, $mode);
	}

}
