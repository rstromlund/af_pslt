<?php

class Anchors extends Pslt_Processor {

	//E.g. :	http://pslt.localhost/anchors?author=Fathom Mag&url=http://www.fathommag.com

	public $seen_hrefs = array();

	public function init($feed, $fetch_url, $fetch_comp, $config_info) {
		parent::init($feed, $fetch_url, $fetch_comp, $config_info);
		$this->seen_hrefs = array();
	}

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;
		$_href = (XML_ELEMENT_NODE === $node->nodeType) ? $node->getAttribute('href') : 'n/a';

		// <html>...</html>
		if ('html' === $_nodeName && XML_ELEMENT_NODE == $node->nodeType) {
			array_push($this->outdom, '<?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/"><channel><description>' . $this->config_info['url'] . ' | Anchors</description><link>' . $this->config_info['url'] . '</link>');
			$r = parent::template($node, $mode);
			array_push($this->outdom, '</channel></rss>');

		}

		// head/title
		else if ('title' === $_nodeName && 'head' === $node->parentNode->nodeName) {
			array_push($this->outdom, '<title>' . $node->nodeValue . '</title>');
			// No need to recurse this leaf node.

		}

		// head/<link rel="icon" type="image/x-icon" href="/files/c/fathom/theme/img/favicon.ico">
		else if ('link' === $_nodeName && 'icon' === $node->getAttribute('rel') && $_href && 'head' === $node->parentNode->nodeName) {
			$url = rewrite_relative_url($this->config_info['url'], $_href);
			array_push($this->outdom, "<image><url><![CDATA[$url]]></url><title>" . $this->config_info['url'] . " | Anchors</title><link>" . $this->config_info['url'] . "</link></image>");
			// No need to recurse this leaf node.

		}

		/*<!-- Elide noise ... -->
xhtml:a[
	'/' = @href
	or $Base = @href
	or starts-with(@href, '#')
	or starts-with(@href, 'mailto:')
	or contains(@href, 'itunes')
	or contains(@href, 'instagram')
	or contains(@href, 'twitter')
	or contains(@href, 'facebook')]
|xhtml:div['thumbnail' = @class or 'story-readlink' = @class]
|text()"
*/

		// <a ... />
		else if ('a' === $_nodeName && (
						'/' === $_href ||
						$this->config_info['url'] === $_href ||
						0 === strncmp($_href, '#', 1) ||
						0 === strncmp($_href, 'mailto:', 7) ||
						strpos($_href, 'itunes') !== false ||
						strpos($_href, 'instagram') !== false ||
						strpos($_href, 'twitter') !== false ||
						strpos($_href, 'facebook') !== false
		)) {
			// Do nothing.

		}

		// <a ... />
		else if ('a' === $_nodeName) {
			$_href = rewrite_relative_url($this->config_info['url'], $_href);

			$nm = trim(preg_replace('!\s{2,}!', ' ', $node->nodeValue));
			if (! $nm) {
				$alt = $this->xpath->query('.//*[@alt]', $node)[0];
				if ($alt) $nm = $alt->getAttribute('alt');
			}

			if (! $nm) $nm = "($_href)";

			if (! in_array($_href, $this->seen_hrefs)) {
				array_push($this->outdom, "<item><guid><![CDATA[$_href]]></guid><link><![CDATA[$_href]]></link><title><![CDATA[$nm]]></title><description><![CDATA[$nm]]></description></item>");
				array_push($this->seen_hrefs, $_href);
			}
		}

		else {
			$r = parent::template($node, $mode);
		}

		return $r;

	}

	public function process_url($url, $mode) {
		_debug("process_url: Loading HTML from : $url");
		$sgml = fetch_file_contents(array('url' => $url));

		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		libxml_use_internal_errors(true);

		// Scripts can contain tags that throw off HTML parsing
		$doc->loadHTML('<?xml encoding="UTF-8">' . preg_replace('~<script[^<>]*[^/]>.*?</script>~su', '', $sgml));

		//FIXME: this ERROR message does not return valid RSS/Atom XML. Create a fake <item> w/ the error? Ignore the false return?
		return ($this->process_doc($doc, $mode)) ? join('', $this->outdom) : "ERROR processing $url :: $mode :: " . join('', $this->outdom) . " :: $sgml";
	}

}
