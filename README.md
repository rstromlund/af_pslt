# "PSLT" (php like xslt). TT-RSS Plugin

Creates a feed from (usually) HTML website using PHP that works similarly to XSLT. New "processors" are easy to create and add to the plugin.

## Anchors:

You can subscribe to just 'anchors' (an item for all the a html elements with appropos link element) from websites (e.g. for CNN Photos) :
 * http://pslt.localhost/anchors?author=CNN+Photos&url=https://www.cnn.com/specials/photos

The anchors processor will look for text or alt attributes to place in the feed, but likely you will want to "slurp" in the actual web page. Suggested feediron pattern if you subscribe to CNN Photos :

```json
"://www.cnn.com/": {
	"type": "xpath",
	"xpath": "body",
	"force_charset": "utf-8",
	"cleanup": [
		"div[contains(@class, 'share-container')]",
		"form", "iframe", "script"
	]
}
```

## Cinemark:

You can subscribe to Cinemark movie listings via (e.g. for Cinemark Tinseltown USA and IMAX in New York) :
 * http://pslt.localhost/cinemark?author=Cinemark&url=https://www.cinemark.com/new-york/cinemark-tinseltown-usa-and-imax

Each movie title is a handy link to movie reviews.

## Feedmorph:

You can change Atom/RSS feeds via feedmorph (to date, this is the only XML processor) :
 * http://pslt.localhost/feedmorph?tn=no&descr=no&url=https://www.youtube.com/feeds/videos.xml?channel_id=UC6107grRI4m0o2-emgoDnAA
   * moves 'media:thumbnail' and 'media:description' into a 'content' element where aggregators expect to find it.
   * remove 'tn=no&' and/or 'descr=no&' from the above subscription url to enable thumbnail and description morphs.
 * http://pslt.localhost/feedmorph?url=https://soylentnews.org/index.rss
   * copies 'slash:department' to 'description' so it shows in aggregators.

Presently, youtube and soylent news (and b/c codebase inheritance: slashdot) are the only morphs. Handy place to add more.

## Instagram:

You can subscribe to instagram accounts via (e.g. for NASA) :
 * http://pslt.localhost/instagram?url=https://www.instagram.com/nasa/

If it is necessary to login to instagram to read an account, create a directory named "data" and add the following to a file named "data/.appauth.php" (e.g. login for NASA feed) :

```php
<?php

global $instagram_auth;
$instagram_auth = array(
	'nasa' => array('id' => 'your_account', 'pwd' => 'your_password'),
	'ISS'  => array('id' => 'your_account2', 'pwd' => 'your_password2')
);
```

You can vary which instagram login reads which feeds (or you can reuse the same login for each, different logins is not necessary).

The instagram processor can replay old posts, but that requires manually creating files with post-id's in them and that is beyond this get-started guide and may (or may not?) be document further elsewhere. But for those interested, I uses chromium and the web tools->network and scrolled to the very beginning of the user's feed and captured the post-id's that way.

## Stock_Ticker:

You can subscribe to stock_ticker quotes via (e.g. for 3M) :
 * http://pslt.localhost/stock_ticker?url=https://money.cnn.com/quote/quote.html?symb=MMM

## Twitter:

You can subscribe to twitter accounts via (e.g. for NASA) :
 * http://pslt.localhost/twitter?author=NASA&url=https://twitter.com/NASA

## Watermark:

You can subscribe to watermark sermons and/or re:gen via :
 * http://pslt.localhost/watermark?url=http://www.watermark.org/resources/messages
 * http://pslt.localhost/watermark?url=http://www.watermark.org/series/120

First subscription url for sermons and the second is for re:gen.

## Wayback:

The "wayback" processor creates an RSS feed with back dated items and links (note there is no web page is loaded, this is a simple slightly dynamic feed) :
 * FoxTrot : http://pslt.localhost/wayback?author=Bill+Amend&title=FoxTrot+Classic+by+Bill+Amend&url=http://www.gocomics.com/foxtrot&leap=6139&urldate=Y/m/d
 * Dilbert : http://pslt.localhost/wayback?author=Scott+Adams&title=Dilbert+Classic+Daily+Strip&url=https://dilbert.com&imgs=/strip&leap=8813&urldate=Y-m-d

 * These are not "back dated", but provide links w/ current date for feediron loading:
   * C&H     : http://pslt.localhost/wayback?author=Bill+Watterson&title=Calvin+and+Hobbes+by+Bill+Watterson&url=https://www.gocomics.com/calvinandhobbes&urldate=Y/m/d
	  * note no 'leap' component defaults to 0, aka. links with today's date.
   * FoxTrot : http://pslt.localhost/wayback?author=Bill+Amend&title=FoxTrot+Classic+by+Bill+Amend&url=http://www.gocomics.com/foxtrot&urldate=Y/m/d&dow=Sunday
	  * note the 'dow=Sunday' filter since current FoxTrot comics only come out on Sundays.
   * Dilbert : http://pslt.localhost/wayback?author=Scott+Adams&title=Dilbert+Daily+Strip&url=https://dilbert.com&imgs=/strip&urldate=Y-m-d
   * Sherman : http://pslt.localhost/wayback?author=Jim+Toomey&title=Sherman's+Lagoon+Comic+feed&url=http://shermanslagoon.com&imgs=/comics&urldate=lMn-j-Y

'leap=1' means jump backwards 1 day, so wayback will create links w/ yesterday's date. Suggested feediron pattern for gocomics :

```json
"gocomics.com/": {
	"type": "xpath",
	"xpath": "meta['og:image' = @property]/@content",
	"start_element": "<img src='",
	"end_element": "' />"
}
```

Suggested feediron pattern for Dilbert :

```json
"dilbert.com/strip/": {
	"type": "xpath",
	"xpath": "img[contains(@class,'img-comic')]",
	"tags": {
		"type": "xpath",
		"replace-tags": true,
		"xpath": "p[contains(@class, 'comic-tags')]",
		"modify": [
			{
				"type": "replace",
				"search": ">Tags<",
				"replace": "><"
			},
			{
				"type": "regex",
				"pattern": "/<[^<>]*>/",
				"replace": ""
			}
		],
		"split": "#"
	}
}
```

Suggested feediron pattern for Sherman's Lagoon :

```json
"shermanslagoon.com/comics/": {
	"type": "xpath",
	"xpath": [
		"div['comic-content' = @class]",
		"div['entry-content' = @class]"
	],
	"cleanup": [
		"form", "iframe", "script"
	]
}
```

## TODO:

 * All configuration per subscription is in the subscribe url; easy but maybe not elegant and maybe not in the spirit of TT-RSS plugins. Move to prefs screen?
 * Create a test suite.
 * Improve (i.e. create) documentation
