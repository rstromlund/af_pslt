<?php

class Twitter extends Pslt_Processor {

	//E.g. : http://pslt.localhost/twitter?author=NASA&url=http://127.0.0.1/tt-rss/plugins.local/pslt/test/twitter-NASA-feed.html

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;
		$_class = (XML_ELEMENT_NODE === $node->nodeType) ? $node->getAttribute('class') : 'n/a';

		// <html>...</html>
		if ('html' === $_nodeName && XML_ELEMENT_NODE === $node->nodeType) {
			array_push($this->outdom, '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"><channel><link>' . $this->config_info['url'] . '</link>');
			$r = parent::template($node, $mode);
			array_push($this->outdom, '</channel></rss>');

		}

		// <!-- Elide some stuff -->
		// <xsl:template match="xhtml:*['button' = name() or 'u-hiddenVisually' = @class]" />
		// <xsl:template match="xhtml:ul|xhtml:div['stream-item-footer' = @class or 'self-thread-context' = @class or contains(@class, 'popup-tagged-users')]" />
		else if ('ul' === $_nodeName || 'button' === $_nodeName || 'script' === $_nodeName || 'iframe' === $_nodeName || 'u-hiddenVisually' === $_class || ('div' === $_nodeName && ('stream-item-footer' === $_class || 'self-thread-context' === $_class || strpos($_class, 'popup-tagged-users') !== false))) {
			// skip this branch of the tree ...

		}

		// head/title
		else if ('title' === $_nodeName && 'head' === $node->parentNode->nodeName) {
			array_push($this->outdom, "<title><![CDATA[$node->nodeValue]]></title><description>$node->nodeValue</description>");
			// No need to recurse this leaf node.

		}

		// <body>
		else if ('body' === $_nodeName) {
			// <img class="ProfileAvatar-image " src="https://pbs.twimg.com/profile_images/1091070803184177153/TI2qItoi_400x400.jpg" alt="NASA">
			$logo = $this->xpath->query('.//img[contains(@class, "ProfileAvatar-image") and @src][1]/@src', $node)[0];

			if (isset($logo)) {
				$url = rewrite_relative_url($this->config_info['url'], $logo->nodeValue);
				array_push($this->outdom, "<image><url><![CDATA[$url]]></url><title>" . $this->config_info['url'] . " | Twitter</title><link>" . $this->config_info['url'] . "</link></image>");
				// No need to recurse this leaf node.

			}

			// Process tweets only, skip the visual cruft: header, banner, footers, etc...
			$r = $this->apply_templates($this->xpath->query('.//div["content" = @class]', $node), 'identity');

		}

		// begin what is mostly an 'identity' template w/ some specific overrides:
		else if ('identity' === $mode) {

			// <div class="content">
			if ('div' === $_nodeName && 'content' === $_class) {
				$anchor = $this->xpath->query('.//a[contains(@class, "permalink")][@href]', $node)[0];
				$href = rewrite_relative_url($this->config_info['url'], $anchor->getAttribute('href'));

				$time = $anchor->getAttribute('title');

				$author = $this->xpath->query(".//div['stream-item-header' = @class]/a[1]//*[contains(@class, 'fullname')]", $node)[0]->nodeValue;

				array_push($this->outdom, "<item><guid>$href</guid><link>$href</link><title><![CDATA[$author @ $time]]></title><description><![CDATA[");
				$r = parent::template($node, $mode);
				array_push($this->outdom, "]]></description></item>");

			}

			// <xsl:template match="xhtml:div[starts-with(@class, 'QuoteTweet') and not(starts-with(@class, 'QuoteTweet-'))]">
			else if ('div' === $_nodeName && 0 === strncmp($_class, 'QuoteTweet', 10) && 0 !== strncmp($_class, 'QuoteTweet-', 11)) {
				array_push($this->outdom, "<blockquote>");
				$r = parent::template($node, $mode);
				array_push($this->outdom, "</blockquote>");

			}

			// <div style="background-image:url(...)">
			else if ('div' === $_nodeName && strpos($node->getAttribute('style'), 'background-image:url') !== false) {
				$url = rewrite_relative_url($this->config_info['url'], substr($node->getAttribute('style'), strpos($node->getAttribute('style'), 'background-image:url') + 22, -2));
				array_push($this->outdom, "<div><em>media thumbnail:</em><br/><img src=\"$url\" />");

			}

			// <xsl:template match="xhtml:div[starts-with(@class, 'QuoteTweet') and not(starts-with(@class, 'QuoteTweet-'))]">
			else if ('s' === $_nodeName || 'span' === $_nodeName || ('div' === $_nodeName && ! $this->xpath->query("img", $node))) {
				// Dont replicate the <s>trike through or endless <span/div> tags.  Include their content though:
				$r = parent::template($node, $mode);

			}

			// <img /> : b/c it is self closing.
			else if ('img' === $_nodeName) {
				$url = rewrite_relative_url($this->config_info['url'], $node->getAttribute('src'));
				$alt = htmlspecialchars($node->getAttribute('alt'));
				$class = $node->getAttribute('class');
				array_push($this->outdom, "<img alt=\"$alt\" src=\"$url\" " . (isset($class) ? "title=\"$class\" " : "") . "/>");

			}

			// ...all other elements...
			else if (XML_ELEMENT_NODE === $node->nodeType) {
				array_push($this->outdom, "<$_nodeName");
				foreach ($node->attributes as $a) {
					if ('dir' !== $a->name) array_push($this->outdom, ' ' . $a->name . '="' . htmlspecialchars($a->value) . '"');
				}
				array_push($this->outdom, ">");
				$r = parent::template($node, $mode);
				array_push($this->outdom, "</$_nodeName>");

			}

			// ...text...
			else if (XML_TEXT_NODE === $node->nodeType) {
				array_push($this->outdom, $node->nodeValue);

			}

		}
		// end what is mostly an 'identity' template.

		else {
			$r = parent::template($node, $mode);

		}

		return $r;

	}

}
