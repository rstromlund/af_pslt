<?php

require_once __DIR__ . '/data/.appauth.php';

class Instagram extends Pslt_Processor {

	//E.g. : http://pslt.localhost/instagram?url=https://instagram.com/aggressorliveaboards/

	public function fetch_with_gzip($ch) {
		$e = @curl_exec($ch);

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$hdr = substr($e, 0, $header_size);
		$resp = substr($e, $header_size);

		if ("\x1F" . "\x8B" . "\x08" == substr($resp, 0, 3)) {
			# Response was gzipped (instagram is blocking user agents who cannot handle compression).
			$resp = gzdecode($resp);
		}

		#Leaving these debug comments in until changes burn in (might need them again).
		#Logger::get()->log_error(E_USER_WARNING, "Page URL: " . curl_getinfo($ch, CURLINFO_EFFECTIVE_URL), __FILE__, __LINE__, $context);
		#Logger::get()->log_error(E_USER_WARNING, "Page Hdrs: $hdr", __FILE__, __LINE__, $context);
		#Logger::get()->log_error(E_USER_WARNING, "Page Data: " . htmlspecialchars($resp), __FILE__, __LINE__, $context);

		return([$hdr, $resp]);
	}

	public function fetch_with_cookies($url) {
		$profile = explode('/', $this->config_info['url'], 5)[3]; // e.g. https://www.instagram.com/tmahlmann/
		global $instagram_auth;
		$auth = $instagram_auth[$profile];
		_debug("profile = $profile, auth? " . (isset($auth) ? "T" : "F"));

		// Initialize cURL
		$ch = curl_init();
		//curl_setopt($ch, CURLOPT_VERBOSE, '1'); //useful at times.
		if ($auth) {
			_debug("Auth using acct: $auth[id]");
			curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . "/data/.cookie-$profile.jar");
			curl_setopt($ch, CURLOPT_COOKIEJAR,  __DIR__ . "/data/.cookie-$profile.jar");
		}
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; Trident/7.0; rv:11.0) like Gecko');
		if (0 === strncmp(getenv('HOSTNAME'), 'SY4-', 4)) curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Ignore cert errors? (@ work: yes b/c middleware).

		$curl_http_headers = [];
		array_push($curl_http_headers, 'X-Requested-With: XMLHttpRequest');
		array_push($curl_http_headers, 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9');
		array_push($curl_http_headers, 'Accept-Encoding: gzip, identity'); # ig now fails w/o this header and allowing gzip :/
		array_push($curl_http_headers, 'Accept-Language: en-US,en;q=0.9');
		array_push($curl_http_headers, 'Origin: https://www.instagram.com');

		curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_http_headers);
		curl_setopt($ch, CURLOPT_REFERER, $url);

		// Try and retrieve the instagram profile page:
		curl_setopt($ch, CURLOPT_URL, $url);
		[$h, $t] = $this->fetch_with_gzip($ch);

		// Maybe we need to be logged in?
		if ($auth && strpos($t, 'not-logged-in') !== false)
		{
			_debug('Attempting to login.');
			preg_match('/\nSet-Cookie:\s*csrftoken=([^;]*)/si', $h, $matches);
			array_push($curl_http_headers, "X-CSRFToken: $matches[1]");

			preg_match('/"rollout_hash":"([^"]*)"/s', $t, $matches);
			array_push($curl_http_headers, "X-Instagram-AJAX: $matches[1]");

			curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_http_headers);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "username=$auth[id]&password=$auth[pwd]&enc_password=$auth[pwd]&queryParams={\"source\":\"auth_switcher\"}&optIntoOneTap=false");
			curl_setopt($ch, CURLOPT_REFERER, 'https://www.instagram.com/accounts/login/?source=auth_switcher');
			curl_setopt($ch, CURLOPT_URL, 'https://www.instagram.com/accounts/login/ajax/');
			[$h, $li] = $this->fetch_with_gzip($ch);

			if (false === strpos($li, 'authenticated": true')) {
				_debug("ERROR failed to login -- " . htmlspecialchars($li));
				Logger::get()->log_error(E_USER_WARNING, "Failed to login -- " . htmlspecialchars($li) . " -- $h -- " . json_encode($curl_http_headers), __FILE__, __LINE__, $context);
				return(false);
			}

			curl_setopt($ch, CURLOPT_REFERER, $url);
			curl_setopt($ch, CURLOPT_POSTFIELDS, null);
			curl_setopt($ch, CURLOPT_POST, null);
			curl_setopt($ch, CURLOPT_URL, $url);
			[$h, $t] = $this->fetch_with_gzip($ch);

		}

		curl_close($ch);
		return($t);

	}

	public function mk_entry($n, $disclaimer = '') {
		$author = $n['owner']['username'];

		$caption = join(' ', array_map(function($a) {return($a['node']['text']);}, $n['edge_media_to_caption']['edges']));
		$caption = preg_replace('/\</su', '&lt;', $caption);
		$caption = preg_replace('/\>/su', '&gt;', $caption);
		$caption = preg_replace('/(\\\\n|\r*\n)/su', '<br/>', $caption);

		$tz = get_pref('USER_TIMEZONE', $_SESSION['uid']);
		if ('Automatic' === $tz) $tz = ini_get('date.timezone');
		if (! $tz) $tz = 'America/Chicago';

		$d = date("Y-m-d H:i:s", convert_timestamp($n['taken_at_timestamp'], 'UTC', $tz));

		$shortcode = $n['shortcode'];
		$href = "https://www.instagram.com/p/$shortcode/";

		$img_srcs = isset($n['edge_sidecar_to_children'])
			? array_map(function($a) {return($a['node']['display_url']);}, $n['edge_sidecar_to_children']['edges'])
			: array($n['display_url'])
		;

		$img = join('<br/>', array_map(function($u) {return("<img src='$u' />");}, $img_srcs));

		$title = "$author @ $d" . ($n['is_video'] ? ' (video)' : '');

		// Make order of elements in outdom consistent for simple string parsing if needed (see article filter).
		array_push($this->outdom, "<item><guid>$href</guid><link>$href</link><title>$title</title><dc:creator>$author</dc:creator>" .
			"<description><![CDATA[$caption<br/>$img$disclaimer]]></description></item>"
		);

	}

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;
		$_class = (XML_ELEMENT_NODE === $node->nodeType) ? $node->getAttribute('class') : 'n/a';

		// <html>...</html>
		if ('post-page' !== $mode && 'html' === $_nodeName && XML_ELEMENT_NODE === $node->nodeType) {
			array_push($this->outdom, '<?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/"><channel><link>' . $this->config_info['url'] . '</link>');
			$r = parent::template($node, $mode);
			array_push($this->outdom, '</channel></rss>');

		}

		// head/title
		else if ('post-page' !== $mode && 'title' === $_nodeName && 'head' === $node->parentNode->nodeName) {
			array_push($this->outdom, '<title>' . trim($node->nodeValue) . '</title><description>' . trim($node->nodeValue) . '</description>');
			// No need to recurse this leaf node.

		}

		// <meta property="og:image" content="https://scontent-dfw5-2.cdninstagram.com/..._n.jpg?_nc_ht=scontent-dfw5-2.cdninstagram.com" />
		else if ('post-page' !== $mode && 'meta' === $_nodeName && 'og:image' === $node->getAttribute('property') && $node->getAttribute('content')) {
			$url = rewrite_relative_url($this->config_info['url'], $node->getAttribute('content'));
			array_push($this->outdom, "<image><url><![CDATA[$url]]></url><title>" . $this->config_info['url'] . " | Instagram</title><link>" . $this->config_info['url'] . "</link></image>");
			// No need to recurse this leaf node.

		}

		// <script type="text/javascript">window._sharedData = ... </script>
		else if ('script' === $node->nodeName && strpos($node->textContent, '"config":{') !== false) {
			$b = strpos($node->textContent, '=')+1;
			$e = strpos($node->textContent, '};');
			$json = substr($node->textContent, $b, $e - $b + 1);

			$j = json_decode($json, true);

			if ('post-page' === $mode)
			{
				// We are filtering a PostPage.

				foreach ($j['entry_data']['PostPage'] as $p) {
					if (isset($p['graphql']['shortcode_media'])) {$this->mk_entry($p['graphql']['shortcode_media']);}
				}

			}
			else if ('post-page' !== $mode) {
				// We are generating a feed from a ProfilePage.

				$disclaimer = "<br/>(only 'frontpage' images reflected in this feed at this time; article filter pending).";

				foreach ($j['entry_data']['ProfilePage'] as $p) {
					$u = $p['graphql']['user'];
					$author = $u['username'];

					if (isset($u['edge_owner_to_timeline_media']['edges']))	{array_map(function ($e) {$this->mk_entry($e['node'], $disclaimer);}, $u['edge_owner_to_timeline_media']['edges']);}
					if (isset($u['edge_felix_video_timeline']['edges']))		{array_map(function ($e) {$this->mk_entry($e['node'], $disclaimer);}, $u['edge_felix_video_timeline']['edges']);}

				}

			}
			else {
				Logger::get()->log_error(E_USER_WARNING, "Unknown mode/state in instagram.template(): $mode", __FILE__, __LINE__, $context);
				$r = false;

			}

		}

		// <script type="text/javascript">window.__additionalDataLoaded ...</script>
		else if ('script' === $node->nodeName && strpos($node->textContent, 'window.__additionalDataLoaded(') !== false) {
			$b = strpos($node->textContent, ',')+1;
			$e = strpos($node->textContent, '});');
			$json = substr($node->textContent, $b, $e - $b + 1);

			$j = json_decode($json, true);

			if ('post-page' === $mode)
			{
				// We are filtering a "additionalDataLoaded" on a PostPage.
				if (isset($j['graphql']['shortcode_media'])) {$this->mk_entry($j['graphql']['shortcode_media']);}
			}
			else if ('post-page' !== $mode) {
				Logger::get()->log_error(E_USER_WARNING, "Strange, never seen an 'additionalDataLoaded' called on the main page? ($mode) -- " . $node->textContent, __FILE__, __LINE__, $context);
			}
			else {
				Logger::get()->log_error(E_USER_WARNING, "Unknown mode/state in instagram.template(): $mode", __FILE__, __LINE__, $context);
				$r = false;

			}

		}

		else {
			$r = parent::template($node, $mode);

		}

		return $r;

	}

	function article_filter($article) {
		$this->outdom = array();
		// mode === 'post-page' stops all the RSS elements, keeps just the item element and its children.
		$xml = $this->process_url($article['link'], 'post-page');

		if ('' !== $xml && $xml !== $article['content']) {
			// Crude way to parse XML, but I just want to grab a couple things.  :/
			preg_match("~<title>([^<>]+)</title>.*?<description><!\[CDATA\[(.*?)\]\]></description>~su", $xml, $elems);
			$article['title']		= $elems[1];
			$article['content']	= $elems[2];
		}

		return $article;

	}

	public function process_url($url, $mode) {
		_debug("process_url: Loading Instagram HTML from : $url ($mode)");

		$profile = explode('/', $this->config_info['url'], 5)[3]; // e.g. https://www.instagram.com/tmahlmann/
		$now = date_create('now America/New_York'); // VPS runs on Eastern time.  Trying to hit update.php round 2.
		$hhmm = $now->format('Hi');

		// If processing the "home" page (not a post page) and in a 4 hour window (allow 1 fetch) and if replay file exists ...

		if ($url === $this->config_info['url'] && $hhmm >= '0900' && $hhmm <= '1300' && $wb = glob(__DIR__ . "/data/$profile/x??")) {
			// ... Then Wayback!: we have old saved posts to replay.
			// 1st update of the day is 0505 and my instagram feeds are every 4 hours each.  So this is window #2 (0505 + 0400 = 0905).
			$ids = $wb[0];
			_debug("Wayback processing " . $ids);

			$this->outdom = array("<?xml version='1.0' encoding='utf-8'?><rss xmlns:dc='http://purl.org/dc/elements/1.1/' version='2.0'><channel><title>$profile | Instagram</title><description>$profile | Instagram</description><link>$url</link>");

			foreach (array_filter(explode("\n", file_get_contents($ids))) as $p) {
				array_push($this->outdom, "<item><guid>https://www.instagram.com/p/$p/</guid><link>https://www.instagram.com/p/$p/</link><title>$profile classic @ $p</title><dc:creator>$profile classic</dc:creator><description>(Wayback filler, filter article should replace)</description></item>");
			}

			_debug("Rename '$ids' tp '$ids-Complete' " . (rename($ids, "$ids-Complete") ? 'Ok' : 'Fail'));

			return(join('', $this->outdom) . '</channel></rss>');

		}

		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		libxml_use_internal_errors(true);

		$doc->loadHTML('<?xml encoding="UTF-8">' . $this->fetch_with_cookies($url));
		return ($this->process_doc($doc, $mode)) ? join('', $this->outdom) : "ERROR processing $url :: $mode :: " . join('', $this->outdom);

	}

}
