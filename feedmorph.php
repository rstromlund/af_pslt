<?php

class Feedmorph extends Pslt_Processor {

	//E.g. :	https://www.youtube.com/feeds/videos.xml?channel_id=UCxzC4EngIsMrPmbm6Nxvb-A
	//E.g. :	https://soylentnews.org/index.rss

	public static $needsCData = array(
		'link', 'title', 'media:title', 'media:description', 'feedburner:origLink',
		'name'
	);

	public static $elide_blocks = array(
		// The Action Lab
		'For more awesome videos checkout:',

		//Engineering Explained
		'Related Videos:',

		//JC Travel Stories
		'QUESTIONS I GET OFTEN',

		//minutephysics
		'REFERENCES',

		//MinuteEarth
		'And visit our website: https://www.minuteearth.com',
		'If you liked this week’s video, you might also like',

		//It's Okay To Be Smart
		'FOLLOW US:',

		//Isaac Arthur
		'Social Media:'
	);

	public static $elide_sentences = array(
		// Vintage Space
		'facebook.com',
		'google.com',
		'instagram.com',
		'twitter.com',
		'youtube.com/channel/',

		'sponsoring',
		'free 30-day trial',
		'from my publisher',
		'in my book',
		' forget to subscribe',
		'Any help',
		'patreon',
		'a patron',
		'My blog ',
		' hardcover ',
		't-shirts',
		'Want weekly ',
		' order your ',
		'Amazon',
		'store.html',

		// BrainCraft
		'Please check out',
		'SUBSCRIBE to',
		'Music by',

		// The Action Lab
		'Action Lab Box',
		'Other Channel:',
		'sponsored by',

		// Bethel Music 
		'Spotify',
		'Itunes',
		'Apple Music',
		'Subscribe for',
		'Stay Connected',

		// Numberphile
		'More links',
		'supported by',
		'support from',
		'Subscribe:',
		'reddit.com',
		'Sign up',

		//TED
		'TED Talks recommended',
		'The TED Talks channel features',
		'Look for talks on',

		//MinutePhysics
		'brilliant.org',
		'MinutePhysics is on twitter',
		'And Google+',
		'http://bit.ly/',
		'Minute Physics provides an energetic and entertaining view',

		//It's Okay To Be Smart
		'curiositystream.com',
		'Go check out',
		'SUBSCRIBE so',
		'Try Audible',
		'More info and sources below',

		//Engineering Explained
		'https://goo.gl'
	);

	public $namespaces = array();
	public $xmlns = '';

	// cleanUp: my attempt at eliminating some of the yt boilerplate almost all channels have. :/  Hopefully I get a good
	//		corpus of "spam" words/phrases and I am not endlessly playing wack-a-mole.

	public function cleanUp($txt) {
		$txtChunck = array_reduce(self::$elide_blocks, function ($carry, $item) { return explode($item, $carry, 2)[0]; }, $txt);

		$bKeep = true;
		$ls = array_filter(
			// Split on sentences and newlines.
			preg_split('/((?:[.!?]\W)|\\n)/u', $txtChunck, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE),

			function ($v) use (&$bKeep) {
				if (! $bKeep && mb_strlen($v) <= 2) {
					// Last item skipped and this is short (a delimiter?).
					$bKeep = true; // for next loop (only elide this 1 delim);
					return(false); // skip this one.
				}
				else {
					$bKeep = true;
					foreach (self::$elide_sentences as $e) { if (mb_stripos($v, $e) !== false) { /*Skip phrase/sentence*/ $bKeep = false; break; } }
				}
				return($bKeep);
			}

		);

		$str = preg_replace('/(?:\\n){1,4}(?:\:\s*)?/su', '<br/>', preg_replace('/(?:\s|\\n)+$/su', '', preg_replace('/^(?:\s|\\n|:)+/su', '', join('', array_values($ls)))));
		return($str);

	}

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;

		// DOMDocument doesnt seem to have a convienient way to retrieve all namespaces used, so recording as we go.
		if (isset($node->namespaceURI) && ! isset($this->namespaces[$node->prefix])) {
			// If DOMXPath supported 'xmlXPathNsLookup' then I wouldnt need to track the namespaces in a dictionary. :/
			$this->namespaces[$node->prefix] = $node->namespaceURI;
			$this->xpath->registerNamespace($node->prefix, $node->namespaceURI);

			$this->xmlns .= ' xmlns' . (isset($node->prefix) && '' !== $node->prefix ? ":$node->prefix" : '') . "=\"$node->namespaceURI\"";
		}


		if (('feed' === $_nodeName || 'rss' === $_nodeName || 'rdf:RDF' === $_nodeName) && 'identity' !== $mode) {
			// Save where this element is being added.
			$n = count($this->outdom);

			// Copy it into the output dom, recording all the namespaces as we go.
			$this->template($node, 'identity');

			// The identity template below conviently leaves the &gt; off the opening tag as it adds attributes.
			// So I can just append the namespaces to the end and voila!
			$this->outdom[$n] .= $this->xmlns;

		}

		else if ('yt:channelId' === $_nodeName && 'feed' === $node->parentNode->nodeName) {
			array_push($this->outdom, "<$_nodeName>$node->nodeValue</$_nodeName>");

			$icon_file = ICONS_DIR . "/$this->feed.ico";
			if (!file_exists($icon_file)) {

				// Easy place to check and see if our favicon needs loading.  Unfortunately yt doesnt include this in the atom feed
				// so we only load the HTML page if the favicon is missing.

				$url = $this->xpath->query('../atom:link["alternate" = @rel]', $node)[0];
				if ($url && $html = fetch_file_contents(array('url' => $url->getAttribute('href')))) {

					libxml_use_internal_errors(true);

					$doc = new DOMDocument();
					$doc->loadHTML($html);
					$xpath = new DOMXPath($doc);

					$icon = $xpath->query('//meta["og:image" = @property]')[0];
					if ($icon) {
						array_push($this->outdom, '<icon>' . $icon->getAttribute('content') . '</icon>');
					}

				}

			}

		}

		// ========== https://www.youtube.com/channel/<channelId>/videos : create a 'content' element from 'media:thumbnail' and 'media:description':
		else if ('entry' === $_nodeName && isset($this->namespaces['yt'])) {
			// This is an entry in a youtube feed.  YT doesnt have a "content" element

			array_push($this->outdom, "<entry>");
			$r = parent::template($node, $mode);

			$tn = $this->xpath->query('media:group/media:thumbnail', $node)[0];
			if ('no' !== $this->config_info['tn']) { $tnHtml = isset($tn) ? '<p><img src="' . $tn->getAttribute('url') . '" alt="Video thumbnail" /></p>' : ''; }

			$descr = $this->xpath->query('media:group/media:description', $node)[0];
			if ('no' !== $this->config_info['descr']) { $descrHtml = isset($descr) ? '<p>' . $this->cleanUp($descr->nodeValue) . '</p>' : ''; }

			array_push($this->outdom, "<content type=\"xhtml\"><![CDATA[$tnHtml$descrHtml]]></content>");
			array_push($this->outdom, "</entry>");

		}

		else if (in_array($_nodeName, self::$needsCData) && ! $node->hasAttributes() && strpos($node->nodeValue, '&') !== false) {
			array_push($this->outdom, "<$_nodeName><![CDATA[$node->nodeValue]]></$_nodeName>");

		}

		else if ('media:content' === $_nodeName && isset($this->namespaces['yt'])) {
			// TT-RSS 'FeedItem_Atom::get_content' incorrectly pulls this as the entry/content.  So just elide it.
			// Do nothing.

		}

		// ========== https://soylentnews.org/index.rss : copy in 'slash:department' if it exists:
		else if ('description' === $_nodeName && isset($this->namespaces['rdf'])) {
			// slashdot puts slash:* last so we have not seen that xmlns yet.  Test "rdf" as a substitute.
			$c = $node->childNodes->item(0);
			if (XML_TEXT_NODE !== $c->nodeType && XML_CDATA_SECTION_NODE !== $c->nodeType) {
				return parent::template($node, $mode);
			}

			$dept = $this->xpath->query('../slash:department', $node)[0];
			if (isset($dept)) {
				$n = (0 === strncmp($c->textContent, '<p class="byline">', 18)) ? 18 : 0;
				$c->insertData($n, "from the <em>$dept->nodeValue</em> dept. ");
			}

			//<div class="share_submission" style="position:relative;"> ...twitter,fb,g+,etc... </div>
			//Since this is text and not DOM, we revert to text find/replace:
			$descr = preg_replace('/<div[^<>]*class="share_submission"[^<>]*>.*?<\/div>/su', '', $c->textContent);
			array_push($this->outdom, "<description><![CDATA[$descr]]></description>");

		}

		else if (XML_ELEMENT_NODE === $node->nodeType) {
			array_push($this->outdom, "<$_nodeName");
			foreach ($node->attributes as $a) {
				array_push($this->outdom, ' ' . $a->name . '="' . htmlspecialchars($a->value) . '"');
			}
			array_push($this->outdom, ">");
			$r = parent::template($node, $mode);
			array_push($this->outdom, "</$_nodeName>");

		}

		else if (XML_TEXT_NODE === $node->nodeType) {
			array_push($this->outdom, $node->nodeValue);

		}

		else if (XML_CDATA_SECTION_NODE === $node->nodeType) {
			array_push($this->outdom, "<![CDATA[$node->nodeValue]]>");

		}

		else {
			$r = parent::template($node, $mode);
		}

		return $r;

	}

}