<?php

/*E.g.
E.g. Use `php -a` to get wayback interval:

php > print date_diff(date_create('now'), date_create('1995-01-11'))->format('%R%a days');
-8813 days
*/

class Wayback extends Pslt_Processor {

	//E.g. :	http://pslt.localhost/wayback?author=Bill Amend&title=FoxTrot by Bill Amend&url=http://www.gocomics.com/foxtrot&urldate=Y/m/d&dow=Sunday
	//			http://pslt.localhost/wayback?author=Jim Toomey&title=(tm) Sherman's Lagoon Comic feed&url=http://shermanslagoon.com&imgs=/comics&urldate=lMn-j-Y

	// This isnt really a PSLT processor.  It generates a 1 <item> rss feed. But this is with all my other custom rss feed generating scripts.
	// It *could* be a really small plugin w/ its own fake 'wayback.localhost' type url but it is so easy to add into this framework. What to do (FIXME?).

	function process() {
		$author	= $this->config_info['author'];
		$title	= $this->config_info['title'];
		$url		= $this->config_info['url'];
		$imgs		= $this->config_info['imgs'];
		$urldate	= $this->config_info['urldate'];
		$leap		= $this->config_info['leap'];
		$dow		= $this->config_info['dow'];

		$tz = get_pref('USER_TIMEZONE', $_SESSION['uid']);
		if ('Automatic' === $tz) $tz = ini_get('date.timezone');
		if (! $tz) $tz = 'America/Chicago';

		//FIXME: this is way ugly :( but I need $n to be in local timezone for formatting below;
		// that is why $tz is coerced to be a real (if possible) local timezone above.  'Automatic' wont cut it here.
		// Surely there is a prettier way in ttrss?

		$n = date_create(date("Y-m-d H:i:s", convert_timestamp(strtotime(isset($leap) ? "$leap days ago" : 'now'), 'UTC', $tz)), new DateTimeZone($tz));
		_debug('Leaping to ' . $n->format('c') . " ($tz)");

		$nY = $n->format('Y'); // 4 digit YYYY
		$nM = $n->format('m'); // 2 digit MM
		$nD = $n->format('d'); // 2 digit DD
		$nJ = $n->format('j'); // 1-2 digits D
		$nF = $n->format('F'); // Month name spelled out
		$nL = $n->format('l'); // Day of week spelled out

		if ('Y/m/d' === $urldate) $urldate = "$nY/$nM/$nD";
		else if ('Y-m-d' === $urldate) $urldate = "$nY-$nM-$nD";
		else if ('lMn-j-Y' === $urldate) $urldate = strtolower($nF) . "-$nJ-$nY"; //e.g. april-1-2019
		else $urldate = 'n/a';

		$xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
<channel>
<title>$title</title><description>$title</description><link>$url</link><dc:creator>$author</dc:creator>
XML;

		// We can filter feeds based (right now only on) day-of-week.  I.e. for the Sunday/Monday/... only feeds.
		_debug("wayback filter: time coordinates are $nL; requested filter " . (isset($dow) ? $dow : '*'));

		if (! isset($dow) || $dow === $nL) {
			_debug('wayback filter: match');

			$xml .= <<<ARTICLE
<item><guid>$url$imgs/$urldate</guid><link>$url$imgs/$urldate</link><title>$nL $nF $nD, $nY</title>
<dc:creator>$author</dc:creator><description><![CDATA[$title for $nL $nF $nD, $nY<br />Click on link or use 'af_comics', 'feediron', or 'feedmod' to replace this space.]]></description></item>
ARTICLE;

		}

		return $xml . '</channel></rss>';

	}

}
