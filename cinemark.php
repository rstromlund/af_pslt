<?php

/* TT-RSS Custom CSS could be:

dl[title = "cinemark movie"] dt img {
   float: left;
}

dl[title = "cinemark movie"] dt {
   clear: left;
   padding-top: 2em;
}

dl[title = "cinemark movie"] dd {
   padding-left: 5em;
}
*/

class Cinemark extends Pslt_Processor {

	//E.g. :	http://pslt.localhost/cinemark?author=Cinemark&url=https://www.cinemark.com/north-texas/cinemark-17-and-imax

	public static $search_url = 'https://duckduckgo.com/html?q=';

	public $title;
	public $theater_nm;
	public $showdate;
	public $positionShowtime;

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;
		$_class = (XML_ELEMENT_NODE === $node->nodeType) ? $node->getAttribute('class') : 'n/a';

		// <html>...</html>
		if ('html' === $_nodeName && XML_ELEMENT_NODE == $node->nodeType) {
			array_push($this->outdom, '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"><channel><link>' . $this->config_info['url'] . '</link>');
			$r = parent::template($node, $mode);
			array_push($this->outdom, '</channel></rss>');

		}

		// head/title
		else if ('title' === $_nodeName && 'head' === $node->parentNode->nodeName) {
			//<title>Cinemark 17 and IMAX - Dallas, TX - Cinemark Theatres</title>
			$this->title = $node->nodeValue;
			_debug("TITLE === $this->title");

			array_push($this->outdom, "<title>$this->title</title>");
			// No need to recurse this leaf node.

		}

		// head/<link rel="shortcut icon" type="image/ico" href="/favicon.ico">
		else if ('link' === $_nodeName && 'shortcut icon' === $node->getAttribute('rel') && $node->getAttribute('href') && 'head' === $node->parentNode->nodeName) {
			$url = rewrite_relative_url($this->config_info['url'], $node->getAttribute('href'));
			array_push($this->outdom, "<image><url><![CDATA[$url]]></url><title>Movies @ $this->title</title><link>" . $this->config_info['url'] . "</link></image>");
			// No need to recurse this leaf node.

		}

		// <h1 class="theatreName">Cinemark 17 and IMAX</h1>
		else if (! isset($this->theater_nm) && 'h1' === $_nodeName && 'theatreName' === $_class) {
			$this->theater_nm = $node->nodeValue;
			_debug("THEATER === $this->theater_nm");

			array_push($this->outdom, "<description>$this->theater_nm</description>");
			// No need to recurse this leaf node.

		}

		// <h1 class="print">Showtimes for Friday, March 15, 2019</h1>
		else if (! isset($this->showdate) && 'h1' === $_nodeName && 'print' === $_class) {
			$this->showdate = $node->nodeValue;
			_debug("SHOWDATE === $this->showdate");
			// No need to recurse this leaf node.

		}

		// <div id="showtimesInner">
		else if ('div' === $_nodeName && 'showtimesInner' === $node->getAttribute('id')) {
			array_push($this->outdom,
				"<item><guid>$this->title @ $this->showdate</guid><link>" . $this->config_info['url'] . "</link><title>$this->showdate</title>" .
					"<description><![CDATA[$this->theater_nm : $this->showdate<br/><div><dl title='cinemark movie'>"
			);

			$r = parent::template($node, $mode);

			$xml = "</dl></div>]]></description></item>";
			array_push($this->outdom, $xml);
		}

		// <div class="showtimeMovieBlock 76312 ">
		else if ('div' === $_nodeName && 0 === strncmp($_class, 'showtimeMovieBlock', 18)) {
			$this->positionShowtime = 0;
			$title = trim($this->xpath->query('(.//h2 | .//h3)', $node)[0]->nodeValue);

			$img_markup = '';
			$img = $this->xpath->query('.//img[contains(@data-srcset, "poster") or contains(@alt, "Poster")]', $node)[0];

			if ($img) {
				$url = rewrite_relative_url($this->config_info['url'], $img->getAttribute('data-srcset'));
				$alt = htmlspecialchars($img->getAttribute('alt'));
				$img_markup = "<img alt=\"$alt\" src=\"$url\" />";
			}

			$title_html = htmlspecialchars($title);
			array_push($this->outdom, "<dt>$img_markup<a href=\"" . self::$search_url . "'rottentomatoes+movie+reviews'+'$title_html'\">$title</a></dt><dd>");
			$r = parent::template($node, $mode);
			array_push($this->outdom, "</dd>");
		}

		// <div class="showtime">
		else if ('div' === $_nodeName && 'showtime' === $_class) {
			$tm = trim($this->xpath->query('a', $node)[0]->nodeValue);

			array_push($this->outdom, ((0 !== $this->positionShowtime) ? ' | ' : '') . $tm);
			++ $this->positionShowtime;
		}

		// <div class="showtimePrintType">
		else if ('div' === $_nodeName && 'showtimePrintType' === $_class) {
			$spec = $this->xpath->query('.//img[contains(@class, "3d") or contains(@class, "imax") or contains(@class, "d-box")]', $node)[0];
			$frist = $this->xpath->query('.//img', $node)[0];

			// This is a bit of an XSLT cheat (tracking state); but I guess this would be solved w/ 'following-sibling::div[1]' or similar.
			if ($this->positionShowtime > 0) array_push($this->outdom, '</dd><dd>');

			array_push($this->outdom, ' <strong>[' . (isset($spec) ? $spec->getAttribute('class') : ($frist ? $frist->getAttribute('class') : '*')) . ']</strong> ');
			$this->positionShowtime = 0;
		}

		else {
			$r = parent::template($node, $mode);
		}

		return $r;

	}

}