<?php

require_once __DIR__ . "/pslt_processor.php";

class Af_Pslt extends Plugin {

	private $host;

	function about() { return array(2.0, 'Creates a feed from (usually) HTML website using PHP that works similarly to XSLT. New "processors" are easy to create and add to the plugin.', 'rodneys_mission'); }

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_FETCH_FEED, $this);
		$host->add_hook($host::HOOK_FEED_BASIC_INFO, $this);
		$host->add_hook($host::HOOK_SUBSCRIBE_FEED, $this);
		$host->add_hook($host::HOOK_PREFS_TAB, $this);
		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this);
	}

	function gen_feed($fetch_url, $feed) {
		$fetch_comp = parse_url($fetch_url);

		if ('pslt.localhost' === $fetch_comp['host']) {
			$class_name = substr($fetch_comp['path'], 1);
			if (!class_exists($class_name)) {
				require_once __DIR__ . "/$class_name.php";
         }

			parse_str($fetch_comp['query'], $config_info);
			$url_comp = parse_url($config_info['url']);

			$pslt = new $class_name();
			$pslt->init($feed, $fetch_url, $fetch_comp, $config_info);
			return $pslt->process();

		}

		_debug("gen_feed: Unrecognized url for pslt plugin : $fetch_url");
		return false;

	}

	/**
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	function hook_fetch_feed($feed_data, $fetch_url, $owner_uid, $feed, $last_article_timestamp, $auth_login, $auth_pass) {
		if (preg_match('#^https?://pslt.localhost/([-a-z0-9_]+)\?#i', $fetch_url)) {
			$feed_data = $this->gen_feed($fetch_url, $feed);
		}

		return $feed_data;
	}

	function hook_subscribe_feed($contents, $url, $auth_login, $auth_pass) {
		if (preg_match('#^https?://pslt.localhost/([-a-z0-9_]+)\?#i', $url))
			return '<?xml version="1.0" encoding="utf-8"?>'; // Make is_html() return false

		return $contents;
	}

	function hook_feed_basic_info($basic_info, $fetch_url, $owner_uid, $feed, $auth_login, $auth_pass) {
		if (preg_match('#^https?://pslt.localhost/([-a-z0-9_]+)\?#i', $fetch_url, $matches)) {
			// Get the basic info from the feed, since it is templated it could be anything.

			$rss = new FeedParser($this->gen_feed($fetch_url, $feed));
			$rss->init();
			if (!$rss->error()) {
				$basic_info = array('title' => $rss->get_title(), 'site_url' => $rss->get_link());
			}
		}

		return $basic_info;
	}

	function hook_article_filter($article) {
		$fetch_url = $article['feed']['fetch_url'];
		$fetch_comp = parse_url($fetch_url);

		if ('pslt.localhost' !== $fetch_comp['host']) {
			return($article); // Only 'pslt' classes can have a 'article_filter' method.
		}

		$class_name = substr($fetch_comp['path'], 1);
		if (!class_exists($class_name)) {
			require_once __DIR__ . "/$class_name.php";
		}

		parse_str($fetch_comp['query'], $config_info);
		$url_comp = parse_url($config_info['url']);

		$pslt = new $class_name();
		$pslt->init($article['feed']['id'], $fetch_url, $fetch_comp, $config_info);
		return $pslt->article_filter($article);

	}

	function hook_prefs_tab($args) {
		if ($args != 'prefFeeds') return;

		print '<div dojoType="dijit.layout.AccordionPane" title="<i class=\'material-icons\'>rss_feed</i> '.__('Feeds supported by Pslt').'">';

		print "<p>" . __("The following Processors are currently supported:") . "</p>";

		print "<ul class='panel panel-scrollable list list-unstyled'>";
		print array_reduce(
			// remove 'init.php' and 'pslt_processor.php' from filename list, they are not pslt classes/processors (they are the framework).
			array_filter(glob(__DIR__ . '/*.php'), function ($fname) {return(0 !== substr_compare($fname, 'init.php', -8) && 0 !== substr_compare($fname, 'pslt_processor.php', -18));}),

			// build html list items for each remaining php filename.
			function ($carry, $fname) {$c = basename($fname, '.php'); return "$carry<li>$c</li>"; }
		);
		print "</ul>";

		print '<p>'.__('Use url <code>http://pslt.localhost/{class}?{config_parms}*</code> to subscribe.').'</p>';
		print '<p>'.__('E.g. for <em>Wayback</em> to read GoComic\'s FoxTrot use <code>http://pslt.localhost/wayback?author=Bill Amend&title=FoxTrot by Bill Amend&url=http://www.gocomics.com/foxtrot&imgs=&leap=&urldate=Y/m/d&dow=Sunday</code>).').'</p>';

		print '<p>'.__('Config parameters <code>imgs</code> isn\'t necessary since it isn\'t used and <code>leap</code> can be set to an integer to \'leap\' backwards in time (i.e. replay classic FoxTrots).').'</p>';

		print '</div>';
	}

	function api_version() {
		return 2;
	}

}
