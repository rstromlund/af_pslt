<?php

class Stock_Ticker extends Pslt_Processor {

	//E.g. :	http://money.cnn.com/quote/quote.html?symb=LUV

	public static $holiday = array(
		/*New Years Day*/				'01 Jan 19', '01 Jan 20', '01 Jan 21',
		/*MLK Day*/						'21 Jan 19', '20 Jan 20', '18 Jan 21',
		/*Washington's Birthday*/	'18 Feb 19', '17 Feb 20', '15 Feb 21',
		/*Good Friday*/				'19 Apr 19', '10 Apr 20', '02 Apr 21',
		/*Memorial Day*/				'27 May 19', '25 May 20', '31 May 21',
		/*Independence Day*/			'04 Jul 19', '03 Jul 20', '05 Jul 21',
		/*Labor Day*/					'02 Sep 19', '07 Sep 20', '06 Sep 21',
		/*Thanksgiving Day*/			'28 Nov 19', '26 Nov 20', '25 Nov 21',
		/*Christmas*/					'25 Dec 19', '25 Dec 20', '24 Dec 21'
	);

	public $symbol;

	public function template(DOMNode $node, $mode) {
		$r = true;

		// These come up a lot
		$_nodeName = $node->nodeName;

		// <html>...</html>
		if ('html' === $_nodeName && XML_ELEMENT_NODE == $node->nodeType) {
			array_push($this->outdom, '<?xml version="1.0" encoding="utf-8"?><rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0"><channel><image><url>https://money.cnn.com/favicon.ico</url><title>money.cnn.com favicon</title><link>https://money.cnn.com/data/markets/</link></image>');
			$r = parent::template($node, $mode);
			array_push($this->outdom, '</channel></rss>');

		}

		// <title>LUV - Southwest Airlines Co Stock quote - CNNMoney.com</title>
		else if ('title' === $_nodeName && 'head' === $node->parentNode->nodeName) {
			$title = $node->nodeValue;

			// First token should be stock symbol:
			$this->symbol = explode(' ', $title, 2)[0];

			array_push($this->outdom, "<title><![CDATA[$title]]></title><description><![CDATA[$title]]></description>");
			// No need to recurse this leaf node.

		}

		// <div class="mod-quoteinfo">
		else if ('div' === $_nodeName && 'mod-quoteinfo' === $node->getAttribute('class')) {
			$company = trim($this->xpath->query('.//h1', $node)[0]->nodeValue);

			$price = $this->xpath->query('.//td["wsod_last" = @class]/span', $node)[0]->nodeValue;
			$price = isset($price) ? $price : $this->xpath->query('.//td["wsod_last" = @class]', $node)[0]->nodeValue;
			$price = isset($price) ? $price : '$ERR';

			$time = $this->xpath->query('.//div["wsod_quoteLabelAsOf" = @class]/span', $node)[0]->nodeValue;
			$time = isset($time) ? $time : '12:00am ET';
			$hr_min = explode(" ", $time, 2)[0];

			$now = date_create("now $hr_min America/New_York");
			$ARTICLE_UPDATED_RFC822 = $now->format(DATE_RFC822); //E.g. Mon, 16 Jan 95 22:08:18 +0000
			$rfc822components = explode(" ", $ARTICLE_UPDATED_RFC822);

			if ('Sat' === $rfc822components[0] && 'Sun' === $rfc822components[0]) {
				_debug("No markets on $rfc822components[0]");
			}
			else if ($rfc822components[4] < '09:30' || $rfc822components[4] > '16:00') {
				_debug("Markets closed at $rfc822components[4]");
			}
			else if (in_array("$rfc822components[1] $rfc822components[2] $rfc822components[3]", self::$holiday)) {
				_debug("Market holiday on $rfc822components[1] $rfc822components[2] $rfc822components[3]");
			}
			else {
				// No stock trading on Sat and Sun, so only create <item> on weekdays.
				array_push($this->outdom, "<item><guid>$this->symbol - $price @ $ARTICLE_UPDATED_RFC822</guid><link>http://money.cnn.com/quote/quote.html?symb=$this->symbol</link><title>$this->symbol - $price - $company - CNNMoney.com</title><dc:creator>$this->symbol (stock-ticker bot)</dc:creator><pubDate>$ARTICLE_UPDATED_RFC822</pubDate><description><![CDATA[");
				$r = parent::template($node, 'identity');
				array_push($this->outdom, "]]></description></item>");
			}

		}

		// begin what is mostly an 'identity' template w/ some specific overrides:
		else if ('identity' === $mode) {

			/*<!-- Elide noise ... -->
			//<xsl:template match="xhtml:div['wsod_fRight' = @class]|xhtml:ul['wsod_quoteNav' = @id]|xhtml:div['wsod_chartControls' = @id]|xhtml:form|xhtml:iframe|xhtml:script|comment()|@shape|@style|attribute()[starts-with(., 'javascript:')]" />
comment()|
xhtml:div['wsod_fRight' = @class]|
xhtml:ul['wsod_quoteNav' = @id]|
xhtml:div['wsod_chartControls' = @id]|
xhtml:form|
xhtml:iframe|
xhtml:script|
(attributes n/a; wont automatically add) @shape|@style|attribute()[starts-with(., 'javascript:')]
*/

			if (XML_COMMENT_NODE === $node->nodeType ||
							'form' === $_nodeName ||
							'iframe' === $_nodeName ||
							'script' === $_nodeName ||
							('div' === $_nodeName && 'wsod_fRight' === $node->getAttribute('class')) ||
							('ul' === $_nodeName && 'wsod_quoteNav' === $node->getAttribute('id')) ||
							('div' === $_nodeName && 'wsod_chartControls' === $node->getAttribute('id'))
			) {
				// Do nothing.

			}

			// <img /> : b/c it is self closing.
			else if ('img' === $_nodeName) {
				$url = rewrite_relative_url($this->config_info['url'], $node->getAttribute('src'));
				$alt = htmlspecialchars($node->getAttribute('alt'));
				array_push($this->outdom, "<img alt=\"$alt\" src=\"$url\" />");

			}

			// ...all other elements...
			else if (XML_ELEMENT_NODE === $node->nodeType) {
				array_push($this->outdom, "<$_nodeName");
				foreach ($node->attributes as $a) {
					array_push($this->outdom, ' ' . $a->name . '="' . htmlspecialchars($a->value) . '"');
				}
				array_push($this->outdom, ">");
				$r = parent::template($node, $mode);
				array_push($this->outdom, "</$_nodeName>");

			}

			// ...text...
			else if (XML_TEXT_NODE === $node->nodeType) {
				array_push($this->outdom, $node->nodeValue);

			}

		}
		// end what is mostly an 'identity' template.

		else {
			$r = parent::template($node, $mode);
		}

		return $r;

	}

}